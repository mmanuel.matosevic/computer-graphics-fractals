package hr.fer.irg.lab2.vjezba4;

public class iPoligon {
    private int v1;
    private int v2;
    private int v3;

    public iPoligon(int v1, int v2, int v3){
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
    }

    public int getV1() {return this.v1;}

    public int getV2() {return this.v2;}

    public int getV3() {return  this.v3;}

    public String toString(){
        return v1 + " " + v2 + " " + v3;
    }
}
