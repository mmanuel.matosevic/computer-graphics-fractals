package hr.fer.irg.lab2.vjezba4;

public class iVrh3D {
    private double x;
    private double y;
    private double z;

    public iVrh3D(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {return this.x;}

    public double getY() {return this.y;}

    public double getZ() {return this.z;}

    public String toString(){
        return x + " " + y + " " + z;
    }
}
