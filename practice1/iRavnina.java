package hr.fer.irg.lab2.vjezba4;

public class iRavnina {
    private double A;
    private double B;
    private double C;
    private double D;

    public iRavnina(double A, double B, double C, double D){
        this.A = A;
        this.B = B;
        this.C = C;
        this.D = D;
    }

    public double getA() {return this.A;}

    public double getB() {return this.B;}

    public double getC() {return this.C;}

    public double getD() {return this.D;}

    public String toString(){
        return A + " " + B + " " + C + " " + D;
    }
}
