package hr.fer.irg.lab4.vjezba8;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import hr.fer.irg.lab2.vjezba4.iPoligon;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Fraktali {
    static {
        GLProfile.initSingleton();
    }

    static GLProfile glprofile = GLProfile.getDefault();
    static GLCapabilities glcapabilities = new GLCapabilities(glprofile);
    static final GLCanvas glcanvas = new GLCanvas(glcapabilities);

    private static int eps, limit;
    private static double umin, umax, vmin, vmax;




    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Upisite prag eps i maksimalan broj iteracija m:");

        eps = sc.nextInt();
        limit = sc.nextInt();

        System.out.println("Upisite podrucje kompleksne ravnine koje se pormatra: ");

        umin = sc.nextDouble();
        umax = sc.nextDouble();
        vmin = sc.nextDouble();
        vmax = sc.nextDouble();




        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {


                glcanvas.addGLEventListener(new GLEventListener(){
                    @Override
                    public void init(GLAutoDrawable glAutoDrawable) {

                    }

                    @Override
                    public void dispose(GLAutoDrawable glAutoDrawable) {

                    }

                    @Override
                    public void reshape(GLAutoDrawable glautodrawable, int x, int y, int width, int height) {
                        GL2 gl2 = glautodrawable.getGL().getGL2(); //Returns the GL pipeline object this GLAutoDrawable uses.
                        gl2.glMatrixMode(GL2.GL_PROJECTION); //Sets the current matrix mode.
                        gl2.glLoadIdentity();  //Load the current matrix with the identity matrix

                        GLU glu = new GLU();  //Provides access to the OpenGL Utility Library (GLU). This library provides standard methods for setting up view volumes, building mipmaps and performing other common operations.
                        glu.gluOrtho2D(0.0f, width, 0.0f, height);

                        gl2.glMatrixMode(GL2.GL_MODELVIEW);  ////Sets the current matrix mode.
                        gl2.glLoadIdentity();  //Load the current matrix with the identity matrix

                        gl2.glViewport(0, 0, width, height); //Entry point to C language function: void glViewport
                    }

                    @Override
                    public void display(GLAutoDrawable glautodrawable) {
                        GL2 gl2	= glautodrawable.getGL().getGL2();  //Returns the GL pipeline object this GLAutoDrawable uses.
                        int width = glautodrawable.getSurfaceWidth();
                        int height = glautodrawable.getSurfaceHeight();

                        System.out.println("Razlucivost zaslona je xmax = " + width + " ymax=" + height);

                        gl2.glClear(GL.GL_COLOR_BUFFER_BIT);  //clear buffers to preset values

                        gl2.glLoadIdentity();  //Load the current matrix with the identity matrix

                        //gl2.glTranslatef(width/2, height/2, 0.0f);

                        gl2.glPointSize(1);
                        gl2.glBegin(GL.GL_POINTS);
                        for(int y = 0; y <= height; y++){
                            for(int x=0; x<=width; x++){
                                double creal = x/(double)width * (umax - umin) + umin;
                                double cimag = y/(double)height * (vmax - vmin) + vmin;
                                Complex c = new Complex(creal, cimag);
                                int n = divergence_test(c, limit);
                                if(n == -1) gl2.glColor3f(0.0f, 0.0f, 0.0f);
                                else{
                                    gl2.glColor3d( ( double ) n/ limit ,
                                            1.0 -(double ) n/ limit / 2.0 ,
                                            0.8 -(double ) n/ limit / 3.0);
                                }
                                gl2.glVertex2f(x, y);
                            }
                        }
                        gl2.glEnd();

                    }


                });

                final JFrame jframe = new JFrame("Transformacija i projekcija");
                jframe.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
                jframe.addWindowListener(new WindowAdapter() {

                    public void windowClosing(WindowEvent windowevent) {
                        jframe.dispose();
                        System.exit(0);
                    }
                });

                jframe.getContentPane().add(glcanvas, BorderLayout.CENTER);
                jframe.setSize(1028, 1028);
                jframe.setVisible(true);
                glcanvas.requestFocusInWindow();
            }

        });
    }

    private static int divergence_test(Complex c, int limit){
        double real = 0;
        double imag = 0;
        Complex z = new Complex(real, imag);
        for(int i = 1; i<= limit; i++){
            double next_re = z.getRe()*z.getRe() - z.getIm()*z.getIm() + c.getRe();
            double next_im = 2*z.getRe() *z.getIm() + c.getIm();
            z.setRe(next_re);
            z.setIm(next_im);
            double modul2 = z.getRe() * z.getRe() + z.getIm() * z.getIm();
            if(modul2>4) return i;
        }
        return -1;
    }









}
